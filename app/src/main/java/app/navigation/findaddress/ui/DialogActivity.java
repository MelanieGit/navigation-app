package app.navigation.findaddress.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.media.ExifInterface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;

import app.navigation.findaddress.R;

public class DialogActivity extends Activity {

	private ImageView imageViewBig;
	private ImageView close,rotate;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dialog);
		getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		Intent intent = getIntent();
		final String uri = intent.getStringExtra("img");
		imageViewBig = findViewById(R.id.imageViewBig);
		rotate = findViewById(R.id.rotate);
		close = findViewById(R.id.close);
		final int[] rotateValue = {0};
		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		rotate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (rotateValue[0]){
					case 0:
						rotateValue[0] += 90;
						break;
					case 90:
						rotateValue[0] += 90;
						break;
					case 180:
						rotateValue[0] += 90;
						break;
					case 270:
						rotateValue[0] = 0;
						break;
				}
				Picasso
						.with(DialogActivity.this)
						.load(uri)
						.rotate(rotateValue[0])
						.into(imageViewBig);

			}
		});
		Picasso
				.with(this)
				.load(uri)
				.into(imageViewBig);

	}
}
