package app.navigation.findaddress.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.R;
import app.navigation.findaddress.model.User;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.Countries;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.OnRegisterEvent;
import app.navigation.findaddress.utils.preference.Preference;

public class RegisterActivity extends Activity {
	private EditText name, surname, email, mobile, password,confirmPassword;
	private Button register;
	private View progress;
	private Spinner spinner, countries;
	ArrayList<String> items = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		findViews();
		initViews();
	}

	public void findViews() {
		name = (EditText) findViewById(R.id.firstname);
		surname = (EditText) findViewById(R.id.surename);
		email = (EditText) findViewById(R.id.email);
		mobile = (EditText) findViewById(R.id.mobile);
		password = (EditText) findViewById(R.id.password);
		confirmPassword = (EditText) findViewById(R.id.confirm_password);
		register = (Button) findViewById(R.id.btnReg);
		progress = (View) findViewById(R.id.progress);
		spinner = (Spinner) findViewById(R.id.sex);
		countries = (Spinner) findViewById(R.id.countries);
		register.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				registeInit();
			}
		});
	}

	public void initViews() {
		items.add(getResources().getString(R.string.gender));
		items.add(getResources().getString(R.string.male));
		items.add(getResources().getString(R.string.female));
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.custom_spiner, items);
		final ArrayAdapter<String> adapterCountries = new ArrayAdapter<String>(this, R.layout.custom_spiner, Countries.getCountries());
		adapter.setDropDownViewResource(R.layout.custom_spiner_dropdown);
		adapterCountries.setDropDownViewResource(R.layout.custom_spiner_dropdown);
		spinner.setAdapter(adapter);
		countries.setAdapter(adapterCountries);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (position==0){
					((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.grey));
				}
				spinner.setSelection(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		countries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//				if (position==0){
//					((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.grey));
//				}
				countries.setSelection(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

	}

	public void registeInit() {
		if (isValid()) {
			progress.setVisibility(View.VISIBLE);
			Rest.getInstance().doRegister(
					name.getText().toString().trim(),
					surname.getText().toString().trim(),
					spinner.getSelectedItem().toString(),
					countries.getSelectedItem().toString(),
					email.getText().toString().trim(),
					mobile.getText().toString().trim(),
					password.getText().toString().trim());
		}
	}

	private boolean isEmailValid(String email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	private boolean isPasswordValid(String password) {
		return password.length() > 5;
	}

	public boolean isValid() {
		boolean valid = false;
		if (!TextUtils.isEmpty(name.getText().toString().trim())) {
			name.setError(null);
			if (!TextUtils.isEmpty(surname.getText().toString().trim())) {
				surname.setError(null);
				if (!TextUtils.isEmpty(email.getText().toString().trim())) {
					email.setError(null);
					if (!TextUtils.isEmpty(password.getText().toString().trim())) {
						password.setError(null);
						if (isEmailValid(email.getText().toString().trim())) {
							email.setError(null);
							if (isPasswordValid(password.getText().toString().trim())) {
								if (password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
									confirmPassword.setError(null);
									valid = true;
								} else {
									confirmPassword.setError(getResources().getString(R.string.error_confirm_password));
								}
							} else {
								password.setError(getResources().getString(R.string.error_short_password));
							}

						} else {
							email.setError(getResources().getString(R.string.email_error_message));
						}

					} else {
						password.setError(getResources().getString(R.string.require_error_message));
					}

				} else {
					email.setError(getResources().getString(R.string.require_error_message));
				}
			} else {
				surname.setError(getResources().getString(R.string.require_error_message));
			}
		} else {
			name.setError(getResources().getString(R.string.require_error_message));
		}
		return valid;
	}

	@Subscribe
	public void onRegisterEvent(OnRegisterEvent onRegisterEvent) {
		progress.setVisibility(View.GONE);
		Toast.makeText(this, getResources().getString(R.string.success_registered), Toast.LENGTH_SHORT).show();
		User user = onRegisterEvent.getUser();
		MainApplication.setUser(user);
		Intent intent = new Intent(this, HomeActivity.class);
		Preference.writePreference(RegisterActivity.this, Constants.EMAIL, user.getEmail());
		Preference.writePreference(RegisterActivity.this, Constants.PASSWORD, user.getPassword());
		Rest.getInstance().updateNotificationToken(Preference.readStringPreference(getApplicationContext(),Constants.TOKEN,"").toString());
		startActivity(intent);
	}


	@Subscribe
	public void onErrorEvent(ErrorEvent errorEvent) {
		progress.setVisibility(View.GONE);
		if (errorEvent.getErrorMessage() != null) {
			Toast.makeText(this, errorEvent.getErrorMessage().getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}
}
