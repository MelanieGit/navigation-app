package app.navigation.findaddress.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;

/**
 * Created by Artyom on 1/19/18.
 */

public abstract class ExFragment extends android.support.v4.app.Fragment implements ViewTreeObserver.OnGlobalLayoutListener {
	protected int real_width, real_height;

	Animation.AnimationListener animationListener = new Animation.AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {

		}

		@Override
		public void onAnimationEnd(Animation animation) {
			fullStart();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {

		}
	};

	private void fullStart() {

	}

	private Float elevator;

	public int getReal_width() {
		return real_width;
	}

	public int getReal_height() {
		return real_height;
	}


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		if (elevator != null) ViewCompat.setElevation(view, elevator);
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onGlobalLayout() {
		if (getView() != null) {
			real_width = getView().getWidth();
			real_height = getView().getHeight();
		}
	}

	public void setElevator(Float e) {
		elevator = e;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		Log.d("DEBUG", "onAttach");
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d("DEBUG", "onActivityCreated");
	}

	@Override
	public void onDetach() {
		Log.d("DEBUG", "onDetach");
		super.onDetach();
	}

	@Override
	public void onStart() {
		super.onStart();


	}

	@Override
	public void onStop() {
		super.onStop();
	}


	public void reload() {

	}

	public void updateViewOnceAgain() {

	}

	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
		Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
		if (animation != null) animation.setAnimationListener(animationListener);
		return animation;
	}
}

