package app.navigation.findaddress.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.R;
import app.navigation.findaddress.adapter.AddressListAdapter;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.utils.AddressesComparator;
import app.navigation.findaddress.utils.bus.DefaultUpdatedEvent;
import app.navigation.findaddress.utils.bus.DeleteAddressEvent;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.GetAddressesEvent;

public class AddressesActivity extends AppCompatActivity implements AddressListAdapter.OnIconsClickListener {
	private ArrayList<NavCode> addressList = new ArrayList<>();
	private AddressListAdapter addressListAdapter;
	private ListView listView;
	private int possition;
	private boolean secondTry = false;
	private View progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addresses);
		findViews();
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.addresses);
		if (MainApplication.getUser() != null) {
			String id = String.valueOf(MainApplication.getUser().getId());
			Rest.getInstance().doGetAddresses(id);
			progress.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Change the map type based on the user's selection.
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void initViews() {

	}

	public void findViews() {
		listView = (ListView) findViewById(R.id.addressList);
		progress = findViewById(R.id.progress);
	}

	public void initList(ArrayList<NavCode> addressList) {

		this.addressList = addressList;
		Collections.sort(addressList, new AddressesComparator());
		addressListAdapter = new AddressListAdapter(this, (ArrayList<NavCode>) addressList);
		listView.setAdapter(addressListAdapter);
		addressListAdapter.setOnIconsClickLisener(this);
	}

	@Override
	public void deleteAddress(final int id) {
		possition = id;
		new AlertDialog.Builder(this)
				.setMessage(getResources().getString(R.string.nikute_dialog_title))
				.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						if (addressList.get(id).isVerified()) {
							Toast.makeText(AddressesActivity.this, getResources().getString(R.string.delete_verify), Toast.LENGTH_SHORT).show();
						} else {
							Rest.getInstance().doDeleteAddress(addressList.get(id).getId());
							if (addressList.get(id).isSelected() && addressList.size() > 1) {
								addressList.get((id == 0) ? 1 : 0).setSelected(true);
								addressListAdapter = new AddressListAdapter(AddressesActivity.this, addressList);
								listView.setAdapter(addressListAdapter);
								addressListAdapter.setOnIconsClickLisener(AddressesActivity.this);
								Rest.getInstance().updateDefault(addressList.get((id == 0) ? 1 : 0));
							}
						}
					}
				})
				.setNegativeButton(android.R.string.no, null).show();
	}

	@Override
	public void selectDefault(int id) {
		for (int i = 0; i < addressList.size(); i++) {
			addressList.get(i).setSelected(false);
		}
		addressList.get(id).setSelected(true);
		addressListAdapter = new AddressListAdapter(this, addressList);
		listView.setAdapter(addressListAdapter);
		addressListAdapter.setOnIconsClickLisener(this);
		Rest.getInstance().updateDefault(addressList.get(id));
	}

	@Override
	public void zoomPhoto(String uri) {
		Intent intent = new Intent(this, DialogActivity.class);
		intent.putExtra("img", uri);
		startActivity(intent);
	}

	@Subscribe
	public void onDeleteAddressEvent(DeleteAddressEvent deleteAddressEvent) {
		addressList.remove(possition);
		addressListAdapter = new AddressListAdapter(this, addressList);
		listView.setAdapter(addressListAdapter);
		addressListAdapter.setOnIconsClickLisener(this);
	}

	@Subscribe
	public void onGetAddresses(GetAddressesEvent getAddressesEvent) {
		if (getAddressesEvent.getAddresses() != null && getAddressesEvent.getAddresses().size() > 0) {
			initList(getAddressesEvent.getAddresses());
			progress.setVisibility(View.GONE);
		} else {
//            if(!secondTry) {
			if (MainApplication.getUser() != null) {
				String id = String.valueOf(MainApplication.getUser().getId());
				Rest.getInstance().doGetAddresses(id);
				secondTry = true;
				progress.setVisibility(View.VISIBLE);
				//  }
			}
		}
	}

	@Subscribe
	public void onDefaultChanged(DefaultUpdatedEvent defaultUpdatedEvent) {
	}

	@Subscribe
	public void onErrorEvent(ErrorEvent errorEvent) {
		Toast.makeText(this, errorEvent.getErrorMessage().getMessage(), Toast.LENGTH_SHORT).show();
		progress.setVisibility(View.GONE);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}
}
