package app.navigation.findaddress.ui;

import android.Manifest;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadrosid.lib.drawroutemap.DrawMarker;
import com.ahmadrosid.lib.drawroutemap.DrawRouteMaps;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.squareup.otto.Subscribe;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;

import app.navigation.findaddress.R;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.utils.AddressToStringFunc;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.DetectedActivityToString;
import app.navigation.findaddress.utils.DisplayTextOnViewAction;
import app.navigation.findaddress.utils.GPSTracker;
import app.navigation.findaddress.utils.LocationToStringFunc;
import app.navigation.findaddress.utils.MyReceiver;
import app.navigation.findaddress.utils.PermissionUtils;
import app.navigation.findaddress.utils.ToMostProbableActivity;
import app.navigation.findaddress.utils.Util;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.GetNotification;
import app.navigation.findaddress.utils.bus.OnGeoFenceEvent;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import pl.charmas.android.reactivelocation2.ReactiveLocationProviderConfiguration;

public class NavigationActivity extends AppCompatActivity implements OnMapReadyCallback {

	private GoogleMap mMap;
	private GPSTracker gpsTracker;
	private LatLng latLng;
	private LatLng targetLatLng;
	private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
	private NavCode navCode;
	private Button go, waze;
	private TextView name, address, description, n;
	private ReactiveLocationProvider locationProvider;
	private final static int REQUEST_CHECK_SETTINGS = 0;
	private final static String TAG = "NavigationActivity";
	private Observable<Location> lastKnownLocationObservable;
	private Observable<Location> locationUpdatesObservable;
	private Observable<ActivityRecognitionResult> activityObservable;
	private Observable<String> addressObservable;
	private ReactiveLocationProvider reactiveLocationProvider;
	private Disposable lastKnownLocationDisposable;
	private Disposable updatableLocationDisposable;
	private Disposable addressDisposable;
	private Disposable activityDisposable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_navigation);
		EventBus.getDefault().register(this);
		navCode = (NavCode) getIntent().getSerializableExtra(Constants.NIKUTE);
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		gpsTracker = new GPSTracker(this);
		go = (Button) findViewById(R.id.go);
		waze = (Button) findViewById(R.id.waze);
		name = (TextView) findViewById(R.id.name);
		n = (TextView) findViewById(R.id.n);
		description = (TextView) findViewById(R.id.description);
		address = (TextView) findViewById(R.id.address);
		if (navCode.getBusinessName() != null && !navCode.getBusinessName().equals("")) {
			name.setText(navCode.getBusinessName());
		} else {
			name.setText(navCode.getContactName());
		}
		description.setText(navCode.getDescription());
		address.setText(navCode.getStreetName());

		go.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (targetLatLng != null){
					Uri gmmIntentUri = Uri.parse("google.navigation:q=" + targetLatLng.latitude + "," + targetLatLng.longitude);
					Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
					mapIntent.setPackage("com.google.android.apps.maps");
					if (Util.appInstalledOrNot("com.google.android.apps.maps", NavigationActivity.this)) {
						startActivityForResult(mapIntent, 1);
					} else {
						Toast.makeText(NavigationActivity.this, R.string.no_navigation_app,
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
		waze.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			if (targetLatLng != null) {
				String uri = "waze://?ll=" + targetLatLng.latitude + "," + targetLatLng.longitude + "&navigate=yes";
				if (Util.appInstalledOrNot("com.waze", NavigationActivity.this)) {
					startActivityForResult(new Intent(android.content.Intent.ACTION_VIEW,
							Uri.parse(uri)), 1);
				} else {
					Toast.makeText(NavigationActivity.this, R.string.no_waze_app,
							Toast.LENGTH_SHORT).show();
				}
			}
			}
		});
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.navigation);

		reactiveLocationProvider = new ReactiveLocationProvider(this);

		locationProvider = new ReactiveLocationProvider(getApplicationContext(), ReactiveLocationProviderConfiguration
				.builder()
				.setRetryOnConnectionSuspended(true)
				.build()
		);

		lastKnownLocationObservable = locationProvider
				.getLastKnownLocation()
				.observeOn(AndroidSchedulers.mainThread());

		final LocationRequest locationRequest = LocationRequest.create()
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
				.setNumUpdates(5)
				.setInterval(100);

		locationUpdatesObservable = locationProvider
				.checkLocationSettings(
						new LocationSettingsRequest.Builder()
								.addLocationRequest(locationRequest)
								.setAlwaysShow(true)
								.build()
				)
				.doOnNext(new Consumer<LocationSettingsResult>() {
					@Override
					public void accept(LocationSettingsResult locationSettingsResult) {
						Status status = locationSettingsResult.getStatus();
						if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
							try {
								status.startResolutionForResult(NavigationActivity.this, REQUEST_CHECK_SETTINGS);
							} catch (IntentSender.SendIntentException th) {
								Log.e("MainActivity", "Error opening settings activity.", th);
							}
						}
					}
				})
				.flatMap(new Function<LocationSettingsResult, Observable<Location>>() {
					@Override
					public Observable<Location> apply(LocationSettingsResult locationSettingsResult) {
						return locationProvider.getUpdatedLocation(locationRequest);
					}
				})
				.observeOn(AndroidSchedulers.mainThread());

		addressObservable = locationProvider.getUpdatedLocation(locationRequest)
				.flatMap(new Function<Location, Observable<List<Address>>>() {
					@Override
					public Observable<List<Address>> apply(Location location) {
						return locationProvider.getReverseGeocodeObservable(location.getLatitude(), location.getLongitude(), 1);
					}
				})
				.map(new Function<List<Address>, Address>() {
					@Override
					public Address apply(List<Address> addresses) {
						return addresses != null && !addresses.isEmpty() ? addresses.get(0) : null;
					}
				})
				.map(new AddressToStringFunc())
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread());

		activityObservable = locationProvider
				.getDetectedActivity(50)
				.observeOn(AndroidSchedulers.mainThread());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Change the map type based on the user's selection.
		switch (item.getItemId()) {
			case android.R.id.home:
				Intent intent = new Intent(this, HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		if (mMap != null) {
			enableMyLocation();
			mMap.getUiSettings().setCompassEnabled(true);
		}
		if (gpsTracker.getIsGPSTrackingEnabled()) {
			latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
			targetLatLng = new LatLng(
					Double.valueOf(navCode.getLat()), Double.valueOf(navCode.getLng())
			);
			drawPath(latLng, targetLatLng);
			gpsTracker.updateGPSCoordinates();
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			// gpsTracker.showSettingsAlert();
		}

	}

	private void drawPath(LatLng latLng1, LatLng latLng2) {

		DrawRouteMaps.getInstance(this)
				.draw(latLng1, latLng2, mMap);
		DrawMarker.getInstance(this).draw(mMap, latLng2, R.drawable.point, "Destination Location");
		LatLngBounds bounds = new LatLngBounds.Builder()
				.include(latLng1)
				.include(latLng2).build();
		Point displaySize = new Point();
		getWindowManager().getDefaultDisplay().getSize(displaySize);
		mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));
		mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
		addGeofence();
	}

	private void enableMyLocation() {
		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {
			// Permission to access the location is missing.
			PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
					android.Manifest.permission.ACCESS_FINE_LOCATION, true);
		} else if (mMap != null) {
			// Access to the location has been granted to the app.
			mMap.setMyLocationEnabled(true);
		}
	}

	private void addGeofence() {
		final GeofencingRequest geofencingRequest = createGeofencingRequest();
		if (geofencingRequest == null) return;

		final PendingIntent pendingIntent = createNotificationBroadcastPendingIntent();

		reactiveLocationProvider
				.removeGeofences(pendingIntent)
				.flatMap(new Function<Status, Observable<Status>>() {
					@Override
					public Observable<Status> apply(Status status) throws Exception {
						return reactiveLocationProvider.addGeofences(pendingIntent, geofencingRequest);
					}

				})
				.subscribe(new Consumer<Status>() {
					@Override
					public void accept(Status addGeofenceResult) {
					}
				}, new Consumer<Throwable>() {
					@Override
					public void accept(Throwable throwable) {
					}
				});
	}

	private GeofencingRequest createGeofencingRequest() {
		try {
			double longitude = targetLatLng.longitude;
			double latitude = targetLatLng.latitude;
			float radius = 62;
			Geofence geofence = new Geofence.Builder()
					.setRequestId("GEOFENCE")
					.setCircularRegion(latitude, longitude, radius)
					.setExpirationDuration(Geofence.NEVER_EXPIRE)
					.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
					.build();
			return new GeofencingRequest.Builder().addGeofence(geofence).build();
		} catch (NumberFormatException ex) {
			// toast("Error parsing input.");
			return null;
		}
	}

	private void toast(String text) {
		Toast.makeText(NavigationActivity.this, text, Toast.LENGTH_SHORT).show();
	}

	private PendingIntent createNotificationBroadcastPendingIntent() {
		return PendingIntent.getBroadcast(this, 0, new Intent(this, MyReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
	}

	@Override
	protected void onStart() {
		super.onStart();
		new RxPermissions(this)
				.request(Manifest.permission.ACCESS_FINE_LOCATION)
				.subscribe(new Consumer<Boolean>() {
					@Override
					public void accept(Boolean granted) throws Exception {
						if (granted) {
							onLocationPermissionGranted();
						} else {
							//    toast("Sorry, no demo without permission...");
						}
					}
				});

	}

	protected void onLocationPermissionGranted() {
		lastKnownLocationDisposable = lastKnownLocationObservable
				.map(new LocationToStringFunc())
				.subscribe(new DisplayTextOnViewAction(n), new ErrorHandler());

		updatableLocationDisposable = locationUpdatesObservable
				.map(new LocationToStringFunc())
				.map(new Function<String, String>() {
					int count = 0;

					@Override
					public String apply(String s) {
						return s + " " + count++;
					}
				})
				.subscribe(new DisplayTextOnViewAction(n), new ErrorHandler());


		addressDisposable = addressObservable
				.subscribe(new DisplayTextOnViewAction(n), new ErrorHandler());

		activityDisposable = activityObservable
				.map(new ToMostProbableActivity())
				.map(new DetectedActivityToString())
				.subscribe(new DisplayTextOnViewAction(n), new ErrorHandler());
	}

	private class ErrorHandler implements Consumer<Throwable> {
		@Override
		public void accept(Throwable throwable) {
			//      Toast.makeText(NavigationActivity.this, "Error occurred.", Toast.LENGTH_SHORT).show();
			Log.d("MainActivity", "Error occurred", throwable);
		}
	}

	private void clearGeofence() {
		reactiveLocationProvider
				.removeGeofences(createNotificationBroadcastPendingIntent())
				.subscribe(new Consumer<Status>() {
					@Override
					public void accept(Status status) throws Exception {
						//  toast("Geofences removed");
					}
				}, new Consumer<Throwable>() {
					@Override
					public void accept(Throwable throwable) throws Exception {
						//  toast("Error removing geofences");
						Log.d(TAG, "Error removing geofences", throwable);
					}
				});
	}

	@Subscribe
	public void geoFenceEvent(OnGeoFenceEvent onGeoFenceEvent) {
		if (navCode != null)
		Rest.getInstance().sendNotification(navCode);
	}

	@Override
	protected void onDestroy() {
		EventBus.getDefault().unregister(this);
		clearGeofence();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		clearGeofence();
	//	finish();
	}

}