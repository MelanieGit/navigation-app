package app.navigation.findaddress.ui;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.ahmadrosid.lib.drawroutemap.DrawMarker;
import com.ahmadrosid.lib.drawroutemap.DrawRouteMaps;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

import app.navigation.findaddress.R;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.GPSTracker;
import app.navigation.findaddress.utils.PermissionUtils;
import app.navigation.findaddress.utils.preference.Preference;

public class ArriveActivity extends AppCompatActivity implements OnMapReadyCallback {

    private Button back,showPhoto;
    private GoogleMap mMap;
    private LatLng target;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private GPSTracker gpsTracker;
    private LatLng latLng;
    private NavCode navCode;
    private final LocationListener mLocationListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrive);
        back = (Button) findViewById(R.id.back);
        showPhoto = (Button) findViewById(R.id.show_photo);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArriveActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        navCode = new Gson().fromJson(Preference.readStringPreference(this, "searchedNavcode", "").toString(),NavCode.class);
        String lat = navCode.getLat();
        String lng = navCode.getLng();

        if (!lat.equals("") && !lng.equals(""))
            target = new LatLng(Double.valueOf(lat), Double.valueOf(lng));
        gpsTracker = new GPSTracker(this);
        if (navCode.getAddressPhoto() != null){
            showPhoto.setVisibility(View.VISIBLE);
            showPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    zoomImage(navCode.getAddressPhoto());
                }
            });
        }
}

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        enableMyLocation();
        if (mMap != null && target != null) {
            mMap.addMarker(new MarkerOptions().position(target));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(target, 25));
            if (gpsTracker.getIsGPSTrackingEnabled()) {
                latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                drawPath(latLng, target);
                gpsTracker.updateGPSCoordinates();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                // gpsTracker.showSettingsAlert();
            }
        }
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    android.Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    private void drawPath(LatLng latLng1, LatLng latLng2) {

        DrawRouteMaps.getInstance(this)
                .draw(latLng1, latLng2, mMap);
      //  DrawMarker.getInstance(this).draw(mMap, latLng2, R.drawable.point, "Destination Location");
        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(latLng1)
                .include(latLng2).build();
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));
    }

    public ArriveActivity() {
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(final Location location) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 25));

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
    }
    public void zoomImage(String uri) {
//        final Dialog photoDialog = new Dialog(this);
//        photoDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        photoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        photoDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog, null));
//
//        ImageView image = (ImageView) photoDialog.findViewById(R.id.imageViewBig);
//
//		Picasso
//
//				.with(this)
//				.load(uri)
//				.into(image, new com.squareup.picasso.Callback() {
//					@Override
//					public void onSuccess() {
//                        photoDialog.show();
//					}
//
//					@Override
//					public void onError() {
//
//					}
//				});
//
//
//        image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                photoDialog.cancel();
//            }
//        });
        Intent intent = new Intent(this, DialogActivity.class);
        intent.putExtra("img", uri);
        startActivity(intent);

    }
}
