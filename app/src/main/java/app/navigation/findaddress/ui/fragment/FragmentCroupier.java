package app.navigation.findaddress.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.HashMap;
import java.util.Stack;

import app.navigation.findaddress.utils.Util;

/**
 * Created by Artyom on 1/19/18.
 */

public class FragmentCroupier {

	HashMap<String, Integer[]> animMap = new HashMap<>();
	public HashMap<String, Fragment> fragments = new HashMap<>();
	Stack<String> backStack = new Stack<>();
	FragmentManager.OnBackStackChangedListener backStackChangedListener;
	FragmentManager fragmentManager;
	int resourceId;
	boolean animationRun = false;
	private String curTag = "";
	private String previosLoadTag = "";

	public FragmentCroupier() {

	}

	public void init(FragmentManager fragmentManager, int resourceId) {
		animMap.clear();
		this.fragmentManager = fragmentManager;
		this.resourceId = resourceId;
	}

	public boolean addFragment(String tag, Fragment fragment) {
		return addFragment(tag, fragment, false);
	}

	public boolean addFragment(String tag, Fragment fragment, boolean replace) {
		return addFragment(tag, fragment, replace, 0, 0);
	}

	public boolean addFragment(String tag, Fragment fragment, boolean replace, int animIn, int animOut) {
		return addFragment(tag, fragment, replace, animIn, animOut, 0, 0);
	}

	public boolean addFragment(String tag, Fragment fragment, boolean replace, int animIn, int animOut, int popAnimIn, int popAnimOut) {
		if (!animationRun) {

			if (animIn != 0 | animOut != 0)
				animMap.put(tag, new Integer[]{animIn, animOut});

			if (!fragments.containsKey(tag))
				fragments.put(tag, fragment);

			if (replace) {
				fragmentManager.beginTransaction().setCustomAnimations(animIn, animOut).replace(resourceId, fragment).commit();
				curTag = tag;
				//backStack.push(curTag);
			}
			if (backStackChangedListener != null) backStackChangedListener.onBackStackChanged();

			return true;
		}
		return false;
	}

	public void removeFragment(Fragment fragment) {
		removeFragment(curTag, fragment);
	}

	public void removeFragment(String tag, Fragment fragment) {
		if (fragments.containsKey(tag)) {
			fragments.remove(tag);
		}
	}

	public Fragment getFragment() {
		return getFragment(curTag);
	}

	public Fragment getFragment(String tag) {
		return fragments.get(tag);
	}

	public void remove() {
		fragmentManager.beginTransaction().remove(getFragment()).commit();
	}

	public void setBackStackChangedListener(FragmentManager.OnBackStackChangedListener backStackChangedListener) {
		this.backStackChangedListener = backStackChangedListener;
	}


	public boolean showFragment(String tag) {
		return showFragment(tag, true);
	}

	public boolean showFragment(String tag, boolean addToBackStack) {
		return showFragment(tag, 0, 0, addToBackStack);
	}

	public boolean showFragment(String tag, int animIn, int animOut) {
		return showFragment(tag, animIn, animOut, true);
	}

	public boolean showFragment(String tag, int animIn, int animOut, boolean addToBackStack) {
		if (!animationRun) {

			try {
				if (getFragment().getActivity() != null)
					Util.hideSoftKeyboard(getFragment().getActivity());
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (fragments.containsKey(tag)) {

				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				if (animMap.containsKey(tag)) {
					Integer[] integers = animMap.get(tag);
					if (animIn == 0)
						animIn = integers[0];
					if (animOut == 0)
						animOut = integers[1];
				}

				fragmentTransaction.setCustomAnimations(animIn, animOut);
				fragmentTransaction.replace(resourceId, fragments.get(tag));
				fragmentTransaction.commit();
				if (previosLoadTag.equals(curTag))
//					fragments.get(tag).updateViewOnceAgain();
				previosLoadTag = curTag;
				curTag = tag;
				if (addToBackStack)
					backStack.push(previosLoadTag);
			}
			return true;
		}
		return false;
	}

	public String getCurTag() {
		return curTag;
	}

	public String getPreviosLoadTag() {
		return previosLoadTag;
	}

	public void back() {
		if (backStack.size() != 0) {
			showFragment(backStack.pop(), false);
		}
	}

	public Stack<String> getBackStack() {
		return backStack;
	}

	public void clearBackStack() {
		backStack.clear();
	}
}


