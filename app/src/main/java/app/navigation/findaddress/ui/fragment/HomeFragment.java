package app.navigation.findaddress.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.R;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.model.User;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.ui.FindActivity;
import app.navigation.findaddress.ui.HomeActivity;
import app.navigation.findaddress.ui.fragment.fragmentListener.FragmentEventListener;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.Util;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.OnGetNavCodeEvent;
import app.navigation.findaddress.utils.bus.OnNavCodeByIdEvent;
import app.navigation.findaddress.utils.preference.Preference;


public class HomeFragment extends Fragment implements View.OnClickListener {
    private FragmentEventListener fragmentEventListener;
    private View add, shareHolder, tvAddAddress, myCodeTitle, progres, find, iconEmail, iconMessage, info;
    private TextView tvNavCode;
    private NavCode mNavCode;
    private AutoCompleteTextView nikuteId;

    @Override
    public void onResume() {
        super.onResume();
        if (MainApplication.getUser() != null) {
            String id = String.valueOf(MainApplication.getUser().getId());
            Rest.getInstance().doGetNavCode(id);
        }
    }

    public void setFragmentEventListener(FragmentEventListener fragmentEventListener) {
        this.fragmentEventListener = fragmentEventListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
        initListeners();

    }

    private void findViews(View v) {
        add = v.findViewById(R.id.image_add_address);
        tvAddAddress = v.findViewById(R.id.tvAddAddress);
        nikuteId = (AutoCompleteTextView) v.findViewById(R.id.nikute_id);
        tvNavCode = (TextView) v.findViewById(R.id.tvNavCode);
        shareHolder = v.findViewById(R.id.share_holder);
        myCodeTitle = v.findViewById(R.id.my_code_title);
        progres = v.findViewById(R.id.progress);
        find = v.findViewById(R.id.find);
        iconEmail = v.findViewById(R.id.icon_email);
        iconMessage = v.findViewById(R.id.icom_message);
        info = v.findViewById(R.id.info);
    }

    private void initListeners() {
        add.setOnClickListener(this);
        find.setOnClickListener(this);
        iconMessage.setOnClickListener(this);
        iconEmail.setOnClickListener(this);
        info.setOnClickListener(this);
    }

    @Subscribe
    public void GetNavCode(OnGetNavCodeEvent onGetNavCodeEvent) {
        if(onGetNavCodeEvent.getNavCode()!= null && onGetNavCodeEvent.getNavCode().getCode()!= null
                && !onGetNavCodeEvent.getNavCode().getCode().equals("")) {
            showNavCodeIfExists(true, onGetNavCodeEvent.getNavCode().getCode());
        }else{
            showNavCodeIfExists(false, "");
        }
    }

    @Subscribe
    public void getNikuteByIdEvent(OnNavCodeByIdEvent onNavCodeByIdEvent) {
        progres.setVisibility(View.GONE);
        NavCode navCode = onNavCodeByIdEvent.getNavCode();
        Intent intent = new Intent(getActivity(), FindActivity.class);
        intent.putExtra(Constants.NIKUTE, navCode);
        Gson gson = new Gson();
        String navCodeJson = gson.toJson(navCode);
        Preference.writePreference(getActivity(),"searchedLat",navCode.getLat());
        Preference.writePreference(getActivity(),"searchedLng",navCode.getLng());
        Preference.writePreference(getActivity(),"searchedNavcode",navCodeJson);
        startActivity(intent);
    }


    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
        String message = errorEvent.getErrorMessage().getMessage();
        if(!errorEvent.getErrorMessage().getMessage().equals("no nikute code")){
           Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            progres.setVisibility(View.GONE);
        }else{
            showNavCodeIfExists(false, "");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public LoaderManager getLoaderManager() {
        return super.getLoaderManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_add_address:
              //  if(fragmentEventListener != null) {
                    ((HomeActivity)getActivity()).addAddress();
               // }
                break;
            case R.id.icom_message:
                sendMessage();
                break;
            case R.id.icon_email:
                sendEmail();
                break;
            case R.id.info:
                Toast toast = Toast.makeText(getActivity(), "Enter here the Nikute Code of a Home/Business you want to go to", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                break;
            case R.id.find:
                progres.setVisibility(View.VISIBLE);
                String id = nikuteId.getText().toString().trim();
                if (!id.equals("")) {
                    Rest.getInstance().doGetNavCodeById(Util.removeSpaces(id));
                } else {
                    progres.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_nikute_required), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void sendMessage() {
        String nikute = tvNavCode.getText().toString().trim();
        if (!nikute.equals("")) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"));
            intent.putExtra("sms_body", nikute);
            intent.putExtra(Intent.EXTRA_TEXT, nikute);
            startActivity(intent);
        }
    }

    public void sendEmail() {
        if (!tvNavCode.getText().toString().trim().equals("")) {
            StringBuilder sb = new StringBuilder();
            sb.append("This is my Nikute Code: " +tvNavCode.getText().toString());
            sb.append('\n');
            sb.append('\n');
            sb.append("The Nikute App helps you:");
            sb.append('\n');
            sb.append('\n');
            sb.append("\t 1.\tFind my home/business");
            sb.append('\n');
            sb.append('\n');
            sb.append("\t 2.\tCreate an address for your home/business");
            sb.append('\n');
            sb.append('\n');
            sb.append("Download the app to join our community");
            sb.append('\n');
            sb.append("https://play.google.com/store/apps/details?id=app.navigation.findaddress");
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "My Address");
            i.putExtra(Intent.EXTRA_TEXT, sb.toString());
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void showNavCodeIfExists(boolean show, String code) {
        if (show) {
            tvNavCode.setVisibility(View.VISIBLE);
            shareHolder.setVisibility(View.VISIBLE);
            myCodeTitle.setVisibility(View.VISIBLE);
            tvAddAddress.setVisibility(View.GONE);
            add.setVisibility(View.GONE);
            tvNavCode.setText(Util.modifyNavCode(code));
        } else {
            tvAddAddress.setVisibility(View.VISIBLE);
            add.setVisibility(View.VISIBLE);
            tvNavCode.setVisibility(View.GONE);
            shareHolder.setVisibility(View.GONE);
            myCodeTitle.setVisibility(View.GONE);
        }
    }

}
