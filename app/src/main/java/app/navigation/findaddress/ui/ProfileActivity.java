package app.navigation.findaddress.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.R;
import app.navigation.findaddress.model.User;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.Util;
import app.navigation.findaddress.utils.bus.AddAddressEvent;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.OnUpdateEvent;
import app.navigation.findaddress.utils.bus.OnUpdatePasswordEvent;
import app.navigation.findaddress.utils.bus.UserDeleteEvent;
import app.navigation.findaddress.utils.preference.Preference;

public class ProfileActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
	private Button save,changePasswd;
	private CheckBox checkBox;
	private TextView delete;
	private EditText firstName, sureName, email, mobile, businessName;
	private User user;
	private View progress;
	private Spinner spinner;
	private boolean isBusiness = false;
	ArrayList<String> items = new ArrayList<>();
	private String tempPass = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		findViews();
		initViews();
		isCompany();
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.edit_profile);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}

	public void findViews() {
		save = (Button) findViewById(R.id.save_btn);
		delete = (TextView) findViewById(R.id.delete_tv);
		changePasswd = (Button) findViewById(R.id.tv_change_passwd);
		firstName = (EditText) findViewById(R.id.user_firstname);
		sureName = (EditText) findViewById(R.id.user_surename);
		mobile = (EditText) findViewById(R.id.user_mobile);
		email = (EditText) findViewById(R.id.user_email);
		progress = (View) findViewById(R.id.progress);
		spinner = (Spinner) findViewById(R.id.sex);
	}

	public void initViews() {
		user = MainApplication.getUser();
		firstName.setText(user.getFirstName());
		sureName.setText(user.getSurName());
		email.setText(user.getEmail());
		mobile.setText(user.getMobile());
		save.setOnClickListener(this);
		delete.setOnClickListener(this);
		changePasswd.setOnClickListener(this);
		items.add(getResources().getString(R.string.gender));
		items.add(getResources().getString(R.string.male));
		items.add(getResources().getString(R.string.female));
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.custom_spiner, items);
		adapter.setDropDownViewResource(R.layout.custom_spiner_dropdown);
		spinner.setAdapter(adapter);
		int genderPosition = 0;
		switch (user.getGender()) {
			case "gender":
				break;
			case "male":
				genderPosition = 1;
				break;
			case "female":
				genderPosition = 2;
				break;

		}
		spinner.setSelection(genderPosition);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0) {
					((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.grey));
				}
				spinner.setSelection(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});


	}

	public void isCompany() {
	}

	private boolean isEmailValid(String email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public boolean isValid() {
		boolean valid = false;
		if (!TextUtils.isEmpty(firstName.getText().toString().trim())) {
			firstName.setError(null);
			if (!TextUtils.isEmpty(sureName.getText().toString().trim())) {
				sureName.setError(null);
				valid = true;
			} else {
				sureName.setError(getResources().getString(R.string.require_error_message));
			}
		} else {
			firstName.setError(getResources().getString(R.string.require_error_message));
		}
		return valid;
	}

	public void saveProfile() {
		if (isValid()) {
			progress.setVisibility(View.VISIBLE);
			Rest.getInstance().doUpdate(
					firstName.getText().toString().trim(),
					sureName.getText().toString().trim(),
					spinner.getSelectedItem().toString().toLowerCase(),
					email.getText().toString().trim(),
					mobile.getText().toString().trim(),
					MainApplication.getUser().getPassword()
					//businessName.getText().toString().trim(),isBusiness
			);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Change the map type based on the user's selection.
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void deleteProfile() {
		dialogDelete();


	}

	public void dialogDelete() {
		AlertDialog diaBox = AskOption();
		diaBox.show();
	}

	private AlertDialog AskOption() {
		AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
				.setTitle(getResources().getText(R.string.delete))
				.setMessage(getResources().getString(R.string.delete_dialog_title))
				.setPositiveButton(getResources().getText(R.string.delete), new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						Rest.getInstance().doDelete(user.getEmail());
						dialog.dismiss();
					}

				})
				.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.create();
		return myQuittingDialogBox;
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.save_btn:
				saveProfile();
				break;
			case R.id.delete_tv:
				deleteProfile();
				break;
			case R.id.tv_change_passwd:
				changePassword();
				break;
		}
	}

	private void changePassword() {
		AlertDialog dialog = changePasswdDialog();
		dialog.show();
	}

	private AlertDialog changePasswdDialog() {
		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		final View dialogView = inflater.inflate(R.layout.change_passwd_dialog, null);
		dialogBuilder.setView(dialogView);

		final EditText newPasswd = (EditText) dialogView.findViewById(R.id.new_passwd);
		final EditText confirmPasswd = (EditText) dialogView.findViewById(R.id.confirm_passwd);
		final Button btnSave = (Button) dialogView.findViewById(R.id.btn_save);
		final Button btnCancel = (Button) dialogView.findViewById(R.id.btn_cancel);

		dialogBuilder.setTitle("Change Password");
		btnSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				if (passwordValid(currentPasswd.getText().toString().trim())){
//					currentPasswd.setText(null);
					if (isNewPasswordValid(newPasswd.getText().toString().trim())){
						if (newPasswd.getText().toString().trim().equals(confirmPasswd
								.getText().toString().trim())){
							Rest.getInstance().doUpdatePassword(MainApplication.getUser().getEmail(),
									newPasswd.getText().toString().trim());
							tempPass = newPasswd.getText().toString().trim();
						}else {
							confirmPasswd.setText("");
							Toast.makeText(ProfileActivity.this, getResources().getString(R.string.error_confirm_password),Toast.LENGTH_SHORT).show();
						}
					}else {
						newPasswd.setText("");
						Toast.makeText(ProfileActivity.this, getResources().getString(R.string.error_short_password),Toast.LENGTH_SHORT).show();
					}
//				}
//				else {
//					currentPasswd.setText("Wrong Password");
//				}
			}
		});

//		dialogBuilder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int whichButton) {
//
//			}
//		});
//		dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int whichButton) {
//				{}
//			}
//		});
		final AlertDialog b = dialogBuilder.create();
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				b.cancel();
			}
		});
		return b;
	}

	private boolean passwordValid(String password) {
		if (password.trim().equals("") && password.equals(MainApplication.getUser().getPassword())) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isNewPasswordValid(String password) {
		return password.length() > 5;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	}

	@Subscribe
	public void updateEvent(OnUpdateEvent onUpdateEvent) {
		MainApplication.setUser(onUpdateEvent.getUser());
		Toast.makeText(this, getResources().getString(R.string.profile_updated_message), Toast.LENGTH_SHORT).show();
		progress.setVisibility(View.GONE);
	}
	@Subscribe
	public void updatePasswordEvent(OnUpdatePasswordEvent onUpdatePasswordEvent) {
		MainApplication.setUser(onUpdatePasswordEvent.getUser());
		Preference.clearAll(getApplicationContext());
		Preference.writePreference(getApplicationContext(), Constants.EMAIL, onUpdatePasswordEvent.getUser().getEmail());
		Preference.writePreference(getApplicationContext(), Constants.PASSWORD, tempPass);
		Toast.makeText(this, getResources().getString(R.string.password_updated_message), Toast.LENGTH_SHORT).show();
		progress.setVisibility(View.GONE);
	}

	@Subscribe
	public void onErrorEvent(ErrorEvent errorEvent) {
		progress.setVisibility(View.GONE);
		Toast.makeText(this, errorEvent.getErrorMessage().getMessage(), Toast.LENGTH_SHORT).show();
	}

	@Subscribe
	public void onDeleteEvent(UserDeleteEvent userDeleteEvent) {
		Preference.clearAll(this);
		MainApplication.setUser(null);
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}
}
