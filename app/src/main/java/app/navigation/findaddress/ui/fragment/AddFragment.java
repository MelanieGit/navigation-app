package app.navigation.findaddress.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Path;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.service.chooser.ChooserTarget;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.R;
import app.navigation.findaddress.model.User;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.ui.DialogActivity;
import app.navigation.findaddress.ui.MapActivity;
import app.navigation.findaddress.ui.fragment.fragmentListener.FragmentEventListener;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.Imageutils;
import app.navigation.findaddress.utils.RoundedBorderTransform;
import app.navigation.findaddress.utils.bus.AddAddressEvent;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.OnUpdateEvent;

/**
 * Created by Artyom on 1/22/18.
 */

public class AddFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener, Imageutils.ImageAttachmentListener {
	private CheckBox checkBox;
	private TextView areaName, streetName, city;
	private EditText addName, addMobile, addCompany, nearTo;
	private Button save, addToMap, addPhoto;
	private View addressText,progress;
	private LatLng lat;
	private String address;
	private String cityName;
	private String state;
	private String country;
	private String postalCode;
	private String knownName;
	private String photoPath;
	private boolean isCompany = false;
	private boolean yes = false;
	private ImageView removePhoto, imageView;
	private Imageutils imageutils;

	FragmentEventListener fragmentEventListener;

	public AddFragment() {

	}

	public void setFragmentEventListener(FragmentEventListener fragmentEventListener) {
		this.fragmentEventListener = fragmentEventListener;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add, container, false);

		imageutils = new Imageutils(getActivity(), this, true);
		return rootView;
	}

	// @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViews(view);
		initViews();
		if (MainApplication.getUser() != null && MainApplication.getUser().getFirstName() != null && MainApplication.getUser().getMobile() != null)
			initPage(MainApplication.getUser().getFirstName(), MainApplication.getUser().getMobile());
	}

	private void findViews(View v) {
		checkBox = (CheckBox) v.findViewById(R.id.business);
		addName = (EditText) v.findViewById(R.id.add_name);
		addMobile = (EditText) v.findViewById(R.id.add_mobile);
		addCompany = (EditText) v.findViewById(R.id.company);
		nearTo = (EditText) v.findViewById(R.id.near_to);
		save = (Button) v.findViewById(R.id.save);
		addPhoto = (Button) v.findViewById(R.id.add_photo);
		addToMap = (Button) v.findViewById(R.id.add_to_map);
		addressText = (View) v.findViewById(R.id.address_layout);
		areaName = (TextView) v.findViewById(R.id.area_name);
		streetName = (TextView) v.findViewById(R.id.street_name);
		city = (TextView) v.findViewById(R.id.city);
		imageView = (ImageView) v.findViewById(R.id.imageView);
		removePhoto = (ImageView) v.findViewById(R.id.removePhoto);
	}


	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	private void initViews() {
		checkBox.setOnCheckedChangeListener(this);
		addToMap.setOnClickListener(this);
		save.setBackground(getResources().getDrawable(R.drawable.rounded_orange_selected));
		save.setOnClickListener(this);
		addPhoto.setOnClickListener(this);
		removePhoto.setOnClickListener(this);

		addName.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				save.setEnabled(yes && !isCompany && s.length() > 0);
				if (yes && !isCompany && s.length() > 0) {
					save.setBackground(getResources().getDrawable(R.drawable.button_selector_orange));
				} else {
					save.setBackground(getResources().getDrawable(R.drawable.rounded_orange_selected));
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		addCompany.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				save.setEnabled(yes && isCompany && s.length() > 0);
				if (yes && isCompany && s.length() > 0) {
					save.setBackground(getResources().getDrawable(R.drawable.button_selector_orange));
				} else {
					save.setBackground(getResources().getDrawable(R.drawable.rounded_orange_selected));
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

	}

	public void initPage(String name, String mobile) {
		addMobile.setText(mobile);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		isCompany = isChecked;
		if (isChecked) {
			addName.setText("");
			addCompany.setEnabled(true);
			addCompany.setHintTextColor(getResources().getColor(R.color.grey));
			addName.setEnabled(false);
			addName.setHintTextColor(getResources().getColor(R.color.grey_light));

		} else {
			addCompany.setEnabled(false);
			addCompany.setText(null);
			addCompany.setHint(getResources().getString(R.string.company));
			addCompany.setHintTextColor(getResources().getColor(R.color.grey_light));
			addName.setHintTextColor(getResources().getColor(R.color.grey));
			addName.setEnabled(true);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.save:
				save.setEnabled(false);
				saveAddress();
				break;
			case R.id.add_photo:
				addPhoto();
				break;
			case R.id.removePhoto:
				removePhoto();
				break;
			case R.id.add_to_map:
				Intent intent = new Intent(getActivity(), MapActivity.class);
				startActivityForResult(intent, Constants.REQUEST_CODE);
				break;

		}

	}

	public void saveAddress() {
		if (MainApplication.getUser() != null) {
			String id = String.valueOf(MainApplication.getUser().getId());
			String latitude = String.valueOf(lat.latitude);
			String longitude = String.valueOf(lat.longitude);
			if (cityName == null)
				cityName = "NKT";
			try {
				Rest.getInstance().addAddress(latitude, longitude, address, country, cityName, id, nearTo.getText().toString(),
						addName.getText().toString(), addMobile.getText().toString(), addCompany.getText().toString(),photoPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void addPhoto() {
		imageutils.imagepicker(1);
	}

	public void removePhoto() {
		imageView.setImageBitmap(null);
		photoPath = null;
		removePhoto.setVisibility(View.GONE);
//		hasPhoto = false;
	}

	public void isBusiness() {
		User user = MainApplication.getUser();
		if (isCompany) {
			if (!addCompany.getText().toString().trim().equals("")) {
				addCompany.setError(null);
			} else {
				addCompany.setError(getResources().getString(R.string.error_company_required));
			}
		}
	}

	public void getGeocoder(LatLng latLng) {
		Geocoder geocoder;
		List<Address> addresses = new ArrayList<>();
		geocoder = new Geocoder(getActivity(), Locale.getDefault());

		try {
			addresses = geocoder.
					getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (addresses != null && addresses.size() > 0) {
			address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
			cityName = addresses.get(0).getLocality();
			state = addresses.get(0).getAdminArea();
			country = addresses.get(0).getCountryName();
			postalCode = addresses.get(0).getPostalCode();
			knownName = addresses.get(0).getFeatureName();
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					yes = true;
					lat = (LatLng) data.getExtras().get(Constants.LOCATION);
					getGeocoder(lat);
					String latitude = String.valueOf(lat.latitude);
					String longitude = String.valueOf(lat.longitude);
					addressText.setVisibility(View.VISIBLE);
					areaName.setText(country);
					streetName.setText(address);
					city.setText(cityName);
					if (!addName.getText().toString().trim().equals("") || !addCompany.getText().toString().trim().equals("")) {
						save.setBackground(getResources().getDrawable(R.drawable.button_selector_orange));
						save.setEnabled(true);
					}
				} else {
					yes = false;
				}

			}
		}
		imageutils.onActivityResult(requestCode, resultCode, data);

	}


	@Subscribe
	public void addAddressEvent(AddAddressEvent addAddressEvent) {
		save.setEnabled(true);
		fragmentEventListener.goHomeFragment();
	}


	@Subscribe
	public void onErrorEvent(ErrorEvent errorEvent) {
		if (!errorEvent.getErrorMessage().getMessage().equals("no nikute code"))
			Toast.makeText(getActivity(), errorEvent.getErrorMessage().getMessage(), Toast.LENGTH_SHORT).show();
	}

	//todo error handling

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Override
	public void image_attachment(int from, String filename, final Bitmap file, final Uri uri) {
		Bitmap bitmap = file;
		String file_name = filename;
		//imageView.setImageBitmap(file);
		if (file!=null) {
			removePhoto.setVisibility(View.VISIBLE);
		}

		String path = Environment.getExternalStorageDirectory() + File.separator + "ImageAttach" + File.separator;
		Picasso
				.with(getActivity())
				.load(uri)
				.fit()
				.centerCrop()
				.transform(new RoundedBorderTransform(30,20))
				.into(imageView);
		imageutils.createImage(file, filename, path, false);

		//for transfering to server
		photoPath = imageutils.getInstancePtah().getPath();

		Log.i("pathPHoto",photoPath);
		imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				zoomImage(uri.toString());
			}
		});

	}

	public void zoomImage(String uri) {
		Intent intent = new Intent(getActivity(), DialogActivity.class);
		intent.putExtra("img", uri.toString());
		startActivity(intent);
	}

}
