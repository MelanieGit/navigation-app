package app.navigation.findaddress.ui;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.R;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.ui.fragment.AddFragment;
import app.navigation.findaddress.ui.fragment.FragmentConstants;
import app.navigation.findaddress.ui.fragment.HomeFragment;
import app.navigation.findaddress.ui.fragment.fragmentListener.FragmentEventListener;
import app.navigation.findaddress.utils.Util;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.preference.Preference;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, FragmentEventListener {
    private final int REQUEST_ACCESS_FINE_LOCATION = 1;
    private final int REQUEST_ACCESS_CORSE_LOCATION = 2;
    private final int REQUEST_ACCESS_CAMERA = 3;
    private final int WRITE_EXTERNAL_STORAGE = 4;
    private final int READ_EXTERNAL_STORAGE = 5;
    private View signOut;
    private TextView version;
    private ImageView plus;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.home_page);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //initFragments();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        findViews();
        initViews();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        showPhoneStatePermission();
    }

    public void findViews() {
        signOut = (View) findViewById(R.id.sign_out);
        plus = (ImageView) findViewById(R.id.plus);
        version = (TextView) findViewById(R.id.version);
    }

    public void initViews() {
        signOut.setOnClickListener(this);
        plus.setOnClickListener(this);
        version.setText(getString(R.string.version) + Util.getVersionCode(this));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //if(getFragmentManager().getBackStackEntryCount() > 0){
            super.onBackPressed();
            // }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_add_address) {
            addAddress();
        } else if (id == R.id.nav_account) {
            editProfile();
        } else if (id == R.id.nav_my_address) {
            //   getAddresses();
            Intent intent = new Intent(this, AddressesActivity.class);
            startActivity(intent);
        } else if (id == R.id.home) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initFragments() {
        HomeFragment fragmentHome = new HomeFragment();
        fragmentHome.setFragmentEventListener(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragmentHome);
        fragmentTransaction.addToBackStack(FragmentConstants.HOME);
        fragmentTransaction.commit();
    }

    public void getAddresses() {
        if (MainApplication.getUser() != null) {
            String id = String.valueOf(MainApplication.getUser().getId());
            Rest.getInstance().doGetAddresses(id);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_out:
                Preference.clearAll(this);
                //  MainApplication.setUser(null);
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.plus:
                addAddress();
                break;
        }
    }

    @Override
    public void addAddress() {
        AddFragment fragmentAdd = new AddFragment();
        fragmentAdd.setFragmentEventListener(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragmentAdd);
        fragmentTransaction.addToBackStack(FragmentConstants.ADD);
        fragmentTransaction.commit();
    }

    public void editProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void goHomeFragment() {
        initFragments();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
        }
    }

    /**
     * check permissins !
     */
    private void showPhoneStatePermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_ACCESS_FINE_LOCATION);
            }
        }
        int permissionCheck1 = ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
            } else {
                requestPermission(Manifest.permission.ACCESS_COARSE_LOCATION, REQUEST_ACCESS_CORSE_LOCATION);
            }
        }

        int permissionCheck2 = ContextCompat.checkSelfPermission(
                this, Manifest.permission.CAMERA);
        if (permissionCheck2 != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
            } else {
                requestPermission(Manifest.permission.CAMERA, REQUEST_ACCESS_CAMERA);
            }
        }
        int permissionCheck3 = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck3 != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE);
            }
        }
        int permissionCheck4 = ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck3 != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE);
            }
        }
    }

}
