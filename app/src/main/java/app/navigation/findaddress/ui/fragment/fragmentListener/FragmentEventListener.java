package app.navigation.findaddress.ui.fragment.fragmentListener;

/**
 * Created by Artyom on 1/19/18.
 */

public interface FragmentEventListener {

	void addAddress();
	void goHomeFragment();

}