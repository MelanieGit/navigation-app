package app.navigation.findaddress.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import app.navigation.findaddress.R;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.Util;

public class FindActivity extends AppCompatActivity implements View.OnClickListener {
	private TextView streetName, nikutecode, description,name;
	private Button goNow, changeAddress;
	private NavCode navCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find);
		navCode = (NavCode) getIntent().getSerializableExtra(Constants.NIKUTE);
		findViews();
		initViews();
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.address);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Change the map type based on the user's selection.
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void findViews() {
		streetName = (TextView) findViewById(R.id.find_street_name);
		name = (TextView) findViewById(R.id.name);
		nikutecode = (TextView) findViewById(R.id.nikuteCode);
		description = (TextView) findViewById(R.id.description);
		changeAddress = (Button) findViewById(R.id.go_now_btn);
		goNow = (Button) findViewById(R.id.change_address_btn);
	}

	public void initViews() {
		streetName.setText(navCode.getStreetName());
		nikutecode.setText(Util.modifyNavCode(navCode.getCode()));
		description.setText(navCode.getDescription());
		goNow.setOnClickListener(this);
		changeAddress.setOnClickListener(this);
		if(navCode.getBusinessName() != null && !navCode.getBusinessName().equals("")){
			name.setText(navCode.getBusinessName());
		}else{
			name.setText(navCode.getContactName());
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.go_now_btn:
				goNavActivity();
				break;
			case R.id.change_address_btn:
				onBackPressed();
				break;
		}

	}

	private void goNavActivity() {
		Intent intent = new Intent(this, NavigationActivity.class);
		intent.putExtra(Constants.NIKUTE, navCode);
		startActivity(intent);
	}
}
