package app.navigation.findaddress.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.R;
import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.GetForgotPasswordEvent;
import app.navigation.findaddress.utils.bus.NoInternetConnection;
import app.navigation.findaddress.utils.bus.OnLoginEvent;
import app.navigation.findaddress.utils.preference.Preference;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText email, password;
    private TextView registration, forgotPass;
    private Button login;
    private View progress,splash;
    private String forgotEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findVeiws();
        initViews();
        checkIsLogin();
    }

    public void findVeiws() {
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        registration = (TextView) findViewById(R.id.tvRegistration);
        forgotPass = (TextView) findViewById(R.id.tvForgotPass);
        login = (Button) findViewById(R.id.login);
        progress = (View) findViewById(R.id.progress);
        splash = findViewById(R.id.splash);
    }

    public void initViews() {
        registration.setOnClickListener(this);
        forgotPass.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValid() {

        if (!email.getText().toString().trim().equals("")
                && !password.getText().toString().trim().equals("")) {
            if (isEmailValid(email.getText().toString().trim())) {
                email.setError(null);
                return true;
            } else {
                email.setError(getResources().getString(R.string.email_error_message));
            }
        } else {
            if (email.getText().toString().trim().equals("")) {
                email.setError(getResources().getString(R.string.require_error_message));
            }
            if (password.getText().toString().trim().equals("")) {
                password.setError(getResources().getString(R.string.require_error_message));
            }
        }
        return false;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                if (isValid()) {
                    Rest.getInstance().doLogin(email.getText().toString(), password.getText().toString());
                    progress.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tvRegistration:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
            case R.id.tvForgotPass:
                setForgotEmailDialog();
                break;
        }
    }

    protected void setForgotEmailDialog() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (isEmailValid(editText.getText().toString().trim())) {
                            editText.setError(null);
                            forgotEmail = editText.getText().toString().trim();
                            Rest.getInstance().doGetForgotPassword(forgotEmail);
                        } else {
                            editText.setError(getResources().getString(R.string.email_error_message));
                            return;
                        }
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    private void checkIsLogin() {
        progress.setVisibility(View.VISIBLE);
        if (!Preference.readStringPreference(LoginActivity.this, Constants.EMAIL, "").equals("")) {
            Rest.getInstance().doLogin(Preference.readStringPreference(LoginActivity.this, Constants.EMAIL, "").toString(),
                    Preference.readStringPreference(LoginActivity.this, Constants.PASSWORD, "").toString());
        } else {
            progress.setVisibility(View.GONE);
            splash.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onLoginEvent(OnLoginEvent onLoginEvent) {
        progress.setVisibility(View.GONE);
        if (onLoginEvent.getUser() != null) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra(Constants.USER, onLoginEvent.getUser());
            MainApplication.setUser(onLoginEvent.getUser());
            if (!email.getText().toString().equals("") && !password.getText().toString().equals("")) {
                Preference.writePreference(LoginActivity.this, Constants.EMAIL, email.getText().toString());
                Preference.writePreference(LoginActivity.this, Constants.PASSWORD, password.getText().toString());
                String token = Preference.readStringPreference(getApplicationContext(), Constants.TOKEN, "").toString();
                if(!token.equals("")) {
                    Rest.getInstance().updateNotificationToken(token);
                }
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
        progress.setVisibility(View.GONE);
        splash.setVisibility(View.GONE);
        if (errorEvent.getErrorMessage() != null) {
            Toast.makeText(this, errorEvent.getErrorMessage().getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onForgotPassEvent(GetForgotPasswordEvent getForgotPasswordEvent) {
        Toast.makeText(this, getResources().getString(R.string.forgot_message), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void noInternetConnection(NoInternetConnection noInternetConnection) {
        Toast.makeText(getApplicationContext(), Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        progress.setVisibility(View.GONE);
        splash.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
