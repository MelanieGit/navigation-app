package app.navigation.findaddress.service;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import app.navigation.findaddress.model.ErrorMessage;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.GetAddressesEvent;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Artyom on 2/1/18.
 */

public class GetAddressesRequest extends Request<ArrayList<NavCode>> {
	public GetAddressesRequest(RestInterface restInterface,String id) {
		super(restInterface.getAddresses(id));
		enqueue();
	}

	@Override
	public void onExResponse(Call<ArrayList<NavCode>> call, Response<ArrayList<NavCode>> response) {
		if (response.code() == Constants.SUCCESS_CODE){
			EventBus.getDefault().post(new GetAddressesEvent(response.body()));
		}else{
			Gson gson = new Gson();
			String errorJson = null;
			try {
				errorJson = response.errorBody().string();
			} catch (IOException e) {
				e.printStackTrace();
			}
			ErrorMessage errorMessage = gson.fromJson(errorJson,ErrorMessage.class);
			EventBus.getDefault().post(new ErrorEvent(errorMessage));
		}

	}

	@Override
	protected void onExFailure(Call<ArrayList<NavCode>> call, Throwable t) {

	}
}
