package app.navigation.findaddress.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;

import app.navigation.findaddress.model.ErrorMessage;
import app.navigation.findaddress.model.User;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.OnRegisterEvent;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Artyom on 1/15/18.
 */

public class RegisterRequest extends Request<User> {
	public RegisterRequest(RestInterface restInterface, JsonObject body) {
		super(restInterface.register(body));
		enqueue();
	}

	@Override
	public void onExResponse(Call<User> call, Response<User> response) {
		if (response.code() == Constants.SUCCESS_CODE ){
			EventBus.getDefault().post(new OnRegisterEvent(response.body()));
		}else{
			Gson gson = new Gson();
				String errorJson = null;
				try {
					errorJson = response.errorBody().string();
			} catch (IOException e) {
				e.printStackTrace();
			}
			ErrorMessage errorMessage = gson.fromJson(errorJson,ErrorMessage.class);
			EventBus.getDefault().post(new ErrorEvent(errorMessage));
		}

	}

	@Override
	protected void onExFailure(Call<User> call, Throwable t) {
		System.out.println();
		EventBus.getDefault().post(new ErrorEvent(new ErrorMessage(400,"Server error")));
	}
}
