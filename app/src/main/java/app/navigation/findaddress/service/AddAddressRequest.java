package app.navigation.findaddress.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;

import app.navigation.findaddress.model.ErrorMessage;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.AddAddressEvent;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Multipart;

/**
 * Created by Artyom on 1/23/18.
 */

public class AddAddressRequest extends Request<NavCode>{

	public AddAddressRequest(RestInterface restInterface, JsonObject body, MultipartBody.Part file) {
		super(restInterface.addAddress(body,file));
		enqueue();
	}

	@Override
	public void onExResponse(Call<NavCode> call, Response<NavCode> response) {
		if (response.code() == Constants.SUCCESS_CODE ){
			EventBus.getDefault().post(new AddAddressEvent());
		}else{
			Gson gson = new Gson();
			String errorJson = null;
			try {
				errorJson = response.errorBody().string();
			} catch (IOException e) {
				e.printStackTrace();
			}
			ErrorMessage errorMessage = gson.fromJson(errorJson,ErrorMessage.class);
			EventBus.getDefault().post(new ErrorEvent(errorMessage));
		}

	}

	@Override
	protected void onExFailure(Call<NavCode> call, Throwable t) {

	}
}
