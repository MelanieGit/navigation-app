package app.navigation.findaddress.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import app.navigation.findaddress.model.ErrorMessage;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Artyom on 2/1/18.
 */

public class UpdatePushTokenRequest extends Request<Void> {
	public UpdatePushTokenRequest(RestInterface restInterface, JsonObject jsonObject) {
		super(restInterface.updatePushToken(jsonObject));
		enqueue();
	}

	@Override
	public void onExResponse(Call<Void> call, Response<Void> response) {
		if (response.code() == Constants.SUCCESS_CODE){
		//	EventBus.getDefault().post(new GetNotification(response.message()));
		}else{
			Gson gson = new Gson();
			String errorJson = null;
			try {
				errorJson = response.errorBody().string();
			} catch (IOException e) {
				e.printStackTrace();
			}
			ErrorMessage errorMessage = gson.fromJson(errorJson,ErrorMessage.class);
			EventBus.getDefault().post(new ErrorEvent(errorMessage));
		}

	}

	@Override
	protected void onExFailure(Call<Void> call, Throwable t) {

	}
}
