package app.navigation.findaddress.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;

import app.navigation.findaddress.model.ErrorMessage;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.DefaultUpdatedEvent;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.GetNotification;
import app.navigation.findaddress.utils.bus.OnNavCodeByIdEvent;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Artyom on 1/23/18.
 */

public class SendNotificationRequest extends Request<Void>{

	public SendNotificationRequest(RestInterface restInterface, JsonObject body) {
		super(restInterface.sendNotification(body));
		enqueue();
	}

	@Override
	public void onExResponse(Call<Void> call, Response<Void> response) {
		EventBus.getDefault().post(new GetNotification());
	}

	@Override
	protected void onExFailure(Call<Void> call, Throwable t) {

	}
}
