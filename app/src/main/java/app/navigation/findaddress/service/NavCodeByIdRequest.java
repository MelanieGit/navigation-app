package app.navigation.findaddress.service;

import com.google.gson.Gson;

import java.io.IOException;

import app.navigation.findaddress.model.ErrorMessage;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.OnNavCodeByIdEvent;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Artyom on 1/29/18.
 */

public class NavCodeByIdRequest extends Request<NavCode> {
	public NavCodeByIdRequest(RestInterface restInterface, String id) {
		super(restInterface.getNavCodeById(id));
		enqueue();
	}

	@Override
	public void onExResponse(Call<NavCode> call, Response<NavCode> response) {
		if (response.code() == Constants.SUCCESS_CODE){
			EventBus.getDefault().post(new OnNavCodeByIdEvent(response.body()));
		}else{
			Gson gson = new Gson();
			String errorJson = null;
			try {
				errorJson = response.errorBody().string();
			} catch (IOException e) {
				e.printStackTrace();
			}
			ErrorMessage errorMessage = gson.fromJson(errorJson,ErrorMessage.class);
			EventBus.getDefault().post(new ErrorEvent(errorMessage));
		}

	}

	@Override
	protected void onExFailure(Call<NavCode> call, Throwable t) {

	}
}
