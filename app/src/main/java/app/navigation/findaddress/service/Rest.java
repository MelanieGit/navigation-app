package app.navigation.findaddress.service;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import app.navigation.findaddress.MainApplication;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.Constants;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Rest {
	private static Rest instance;
	private Context context;
	private String serverName;
	private String hostName;
	Retrofit retrofit;
	RestInterface restInterface;


	public Rest(Context context) {
		this.context = context;
		instance = this;
		serverName = "https://api.nikute.com:8443";
//		serverName = "http://192.168.1.4:9000";
		//serverName = "http://10.1.3.251:9000";

		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
				.connectTimeout(60, TimeUnit.SECONDS)
				.readTimeout(60, TimeUnit.SECONDS)
				.writeTimeout(60, TimeUnit.SECONDS)
				.addInterceptor(interceptor);

		OkHttpClient okHttpClient = okHttpBuilder.build();

		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
				.setLenient()
				.create();

		retrofit = new Retrofit.Builder()
				.baseUrl(serverName)
				.client(okHttpClient)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();

		restInterface = retrofit.create(RestInterface.class);
	}

	public static synchronized Rest getInstance() {
		return instance;
	}

	public void doLogin(String email, String password) {
		JsonObject body = new JsonObject();
		body.addProperty(Constants.EMAIL, email);
		body.addProperty(Constants.PASSWORD, password);
		new LoginRequest(restInterface, body);

	}

	public void doRegister(String firsName, String surName, String gender,String country,  String email, String mobile, String password) {
		JsonObject body = new JsonObject();
		body.addProperty(Constants.FIRST_NAME, firsName);
		body.addProperty(Constants.SUR_NAME, surName);
		body.addProperty(Constants.GENDER, gender);
		body.addProperty(Constants.COUNTRY, country);
		body.addProperty(Constants.EMAIL, email);
		body.addProperty(Constants.MOBILE, mobile);
		body.addProperty(Constants.PASSWORD, password);
		new RegisterRequest(restInterface, body);
	}

	public void addAddress(String lat, String lng, String streetName, String areaName, String city,
						   String ownerId, String description, String contactName, String contactNumber,
						   String businessName, String addressPhoto) throws IOException {
		JsonObject body = new JsonObject();

		body.addProperty(Constants.LAT, lat);
		body.addProperty(Constants.LNG, lng);
		body.addProperty(Constants.STREET_NAME, streetName);
		body.addProperty(Constants.AREA_NAME, areaName);
		body.addProperty(Constants.CITY, city);
		body.addProperty(Constants.OWNER_ID, ownerId);
		body.addProperty(Constants.DESCRIPTION, description);
		body.addProperty(Constants.CONTACT_NAME, contactName);
		body.addProperty(Constants.CONTACT_NUMBER, contactNumber);
		body.addProperty(Constants.BUSINESS_NAME, businessName);
		body.addProperty(Constants.ADDRESS_PHOTO, addressPhoto);
		MultipartBody.Part fileToUpload = null;
		if (addressPhoto != null ){
			File file = new File(addressPhoto);
			File file2 = new Compressor(context).compressToFile(file);
			RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file2);
			fileToUpload = MultipartBody.Part.createFormData("photo", file2.getName(), mFile);
		}
//		RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
		new AddAddressRequest(restInterface, body,fileToUpload);
	}

	public void doGetNavCode(String id) {
		new NavCodeRequest(restInterface, id);
	}

	public void doGetAddresses(String id) {
		new GetAddressesRequest(restInterface, id);
	}

	public void doGetForgotPassword(String email) {
		new ForgotPasswordRequest(restInterface, email);
	}

	public void doGetNavCodeById(String id) {
		new NavCodeByIdRequest(restInterface, id);
	}

	public void doDeleteAddress(long id) {
		new DeleteAddressRequest(restInterface, id);
	}

	public void doUpdate(String firsName, String surName, String gender,
						 String email, String mobile, String password) {
		JsonObject body = new JsonObject();
		body.addProperty(Constants.FIRST_NAME, firsName);
		body.addProperty(Constants.SUR_NAME, surName);
		body.addProperty(Constants.GENDER, gender);
		body.addProperty(Constants.EMAIL, email);
		body.addProperty(Constants.MOBILE, mobile);
		body.addProperty(Constants.PASSWORD, password);
		//body.addProperty(Constants.BUSINESS_NAME,password);
		//body.addProperty(Constants.COMPANY,company);
		new UpdateRequest(restInterface, body);
	}
	public void doUpdatePassword(String email,String password) {
		JsonObject body = new JsonObject();
		body.addProperty(Constants.EMAIL, email);
		body.addProperty(Constants.PASSWORD, password);
		new UpdatePassword(restInterface, body);
	}

	public void doDelete(String email) {
		new DeleteRequest(restInterface, email);
	}

	public void updateDefault(NavCode navCode){
		JsonObject body = new JsonObject();
		body.addProperty(Constants.OWNER_ID, navCode.getOwnerId());
		body.addProperty(Constants.CODE, navCode.getCode());
		new UpdateDefaultRequest(restInterface, body);
	}
	public void sendNotification(NavCode navCode){
		if(MainApplication.getUser()!= null) {
			JsonObject body = new JsonObject();
			body.addProperty(Constants.OWNER_ID, navCode.getOwnerId());
			body.addProperty(Constants.NAV_CODE_ID, navCode.getId());
			body.addProperty(Constants.REQUESTER_ID, MainApplication.getUser().getId());
			new SendNotificationRequest(restInterface, body);
		}
	}

	public void updateNotificationToken(String token){
		JsonObject body = new JsonObject();
		body.addProperty(Constants.USER_ID, MainApplication.getUser().getId());
		body.addProperty(Constants.TOKEN, token);
		new UpdatePushTokenRequest(restInterface,body);
	}

	public void addTracking(String status, String date, String navCodeID){
		JsonObject body = new JsonObject();
		body.addProperty(Constants.STATUS, status);
		body.addProperty(Constants.ENTER_DATE, date);
		body.addProperty(Constants.NAVCODE_ID, navCodeID);
		new AddTrackingRequest(restInterface,body);
	}
}
