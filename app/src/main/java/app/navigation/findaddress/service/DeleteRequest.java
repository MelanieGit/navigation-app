package app.navigation.findaddress.service;

import com.google.gson.Gson;

import java.io.IOException;

import app.navigation.findaddress.model.ErrorMessage;
import app.navigation.findaddress.utils.Constants;
import app.navigation.findaddress.utils.bus.ErrorEvent;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.UserDeleteEvent;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Artyom on 1/30/18.
 */

public class DeleteRequest extends Request<Void> {
	public DeleteRequest(RestInterface restInterface,String email) {
		super(restInterface.deleteUser(email));
		enqueue();
	}

	@Override
	public void onExResponse(Call<Void> call, Response<Void> response) {
		if (response.code()== Constants.SUCCESS_CODE){
			EventBus.getDefault().post(new UserDeleteEvent());
		}else {
			Gson gson = new Gson();
			String errorJson = null;
			try {
				errorJson = response.errorBody().string();
			} catch (IOException e) {
				e.printStackTrace();
			}
			ErrorMessage errorMessage = gson.fromJson(errorJson,ErrorMessage.class);
			EventBus.getDefault().post(new ErrorEvent(errorMessage));
		}
	}

	@Override
	protected void onExFailure(Call<Void> call, Throwable t) {

	}
}
