package app.navigation.findaddress.service;

import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;

import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.model.User;
import app.navigation.findaddress.utils.Constants;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface RestInterface {
	@POST("/users/auth")
	Call<User> login(@Body JsonObject json);

	@POST("/users")
	Call<User> register(@Body JsonObject json);

	@Multipart
	@POST("/nav_code")
	Call<NavCode> addAddress( @Part("description") JsonObject json,
							  @Part MultipartBody.Part file );

	@GET("nav_code/{id}")
	Call<NavCode> getNavCode(@Path("id") String id );

	@GET("/nav_code/get_all/{id}")
	Call<ArrayList<NavCode>> getAddresses(@Path("id") String id );

	@POST("/users/update")
	Call<User> update(@Body JsonObject json);

	@POST("users/update_password")
	Call<User> updatePassword(@Body JsonObject json);

	@FormUrlEncoded
	@POST("/nav_code/get_by_code")
	Call<NavCode> getNavCodeById(@Field(Constants.ID) String id);

	@FormUrlEncoded
	@POST("/users/delete")
	Call<Void> deleteUser(@Field(Constants.EMAIL) String email);

	@FormUrlEncoded
	@POST("/users/forgot")
	Call<Void> getForgotPassword(@Field(Constants.EMAIL) String email);


	@DELETE("/nav_code/delete/{id}")
	Call<Void> deleteAddress(@Path("id") long id);

	@POST("/nav_code/set_default")
	Call<NavCode> setDefault(@Body JsonObject json);

	@POST("/events/set_notification")
	Call<Void> sendNotification(@Body JsonObject json);

	@POST("/users/update_push_token")
	Call<Void> updatePushToken(@Body JsonObject json);

	@POST("/tracking")
	Call<Void> addTracking(@Body JsonObject json);
}
