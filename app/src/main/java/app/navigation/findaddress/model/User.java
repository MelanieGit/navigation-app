package app.navigation.findaddress.model;

import java.io.Serializable;

public class User implements Serializable {
	private Long id;

	private String firstName;

	private String surName;

	private String email;

	private String mobile;

	private String password;

	private String gender;

	private String country;

	public User() {
	}

	public User(String firstName, String surName, String email, String mobile, String password) {
		this.firstName = firstName;
		this.surName = surName;
		this.email = email;
		this.mobile = mobile;
		this.password = password;
	}

	public User(Long id, String firstName, String surName, String email, String mobile, String password, String gender, String country) {
		this.id = id;
		this.firstName = firstName;
		this.surName = surName;
		this.email = email;
		this.mobile = mobile;
		this.password = password;
		this.gender = gender;
		this.country = country;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
