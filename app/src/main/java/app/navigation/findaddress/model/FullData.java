package app.navigation.findaddress.model;

/**
 * Created by melaniah on 2/23/2018.
 */

public class FullData {

     private User requester;
    private NavCode navCode;

    public FullData(User requester, NavCode navCode) {
        this.requester = requester;
        this.navCode = navCode;
    }

    public User getRequester() {
        return requester;
    }

    public void setRequester(User requester) {
        this.requester = requester;
    }

    public NavCode getNavCode() {
        return navCode;
    }

    public void setNavCode(NavCode navCode) {
        this.navCode = navCode;
    }
}

