package app.navigation.findaddress.model;

import java.io.Serializable;

/**
 * Created by Artyom on 1/23/18.
 */

public final class NavCode implements Serializable {

	private long id;

	private String code;

	private String lat;

	private String lng;

	private String streetName;

	private String areaName;

	private String city;

	private String description;

	private long ownerId;

	private String businessName;

	private String contactName;

	private String contactNumber;

	private Boolean company;

	private boolean selected;

	private boolean verified;

	private String addressPhoto;

	public NavCode(long id, String code, String lat, String lng, String streetName, String areaName,
				   String city, String description, long ownerId, String businessName,
				   String contactName, String contactNumber, Boolean company, boolean selected,
				   boolean verified, String addressPhoto) {
		this.id = id;
		this.code = code;
		this.lat = lat;
		this.lng = lng;
		this.streetName = streetName;
		this.areaName = areaName;
		this.city = city;
		this.description = description;
		this.ownerId = ownerId;
		this.businessName = businessName;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
		this.company = company;
		this.selected = selected;
		this.verified = verified;
		this.addressPhoto = addressPhoto;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public Boolean getCompany() {
		return company;
	}

	public void setCompany(Boolean company) {
		this.company = company;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String kgetLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLat() {
		return lat;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getAddressPhoto() {
		return addressPhoto;
	}

	public void setAddressPhoto(String addressPhoto) {
		this.addressPhoto = addressPhoto;
	}
}
