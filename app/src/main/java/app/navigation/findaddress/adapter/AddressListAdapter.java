package app.navigation.findaddress.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import app.navigation.findaddress.R;
import app.navigation.findaddress.model.NavCode;
import app.navigation.findaddress.utils.RoundedBorderTransform;
import app.navigation.findaddress.utils.Util;

/**
 * Created by Artyom on 2/1/18.
 */

public class AddressListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<NavCode> addresses = new ArrayList<>();
	private OnIconsClickListener onIconsClickListener;
	private final int ITEM_TYPE_SELECTED = 1;
	private final int ITEM_TYPE_UNSELECTED = 2;

	public AddressListAdapter(Context context, ArrayList<NavCode> addresses) {
		this.context = context;
		this.addresses = addresses;
	}

	@Override
	public int getCount() {
		return addresses.size();
	}

	@Override
	public Object getItem(int position) {
		return addresses.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("ResourceType")
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final Holder holder;
		if (view == null) {
			LayoutInflater layoutInflater = LayoutInflater.from(context);
			view = layoutInflater.inflate(R.layout.address_list_item, null);
			holder = new Holder();
			holder.streetName = (TextView) view.findViewById(R.id.streetName);
			holder.name = (TextView) view.findViewById(R.id.areaName);
			//	holder.city = (TextView) view.findViewById(R.id.city);
			holder.description = (TextView) view.findViewById(R.id.description);
			holder.code = (TextView) view.findViewById(R.id.code);
			holder.deleteBtn = (ImageView) view.findViewById(R.id.deleteBtn);
			holder.addressPhoto = (ImageView) view.findViewById(R.id.address_photo);
			holder.bg = view.findViewById(R.id.bg);
			holder.verified = view.findViewById(R.id.verified);
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
		holder.streetName.setText(addresses.get(position).getStreetName());
		if (addresses.get(position).getBusinessName() != null && !addresses.get(position).getBusinessName().equals("")) {
			holder.name.setText(addresses.get(position).getBusinessName());
		} else {
			holder.name.setText(addresses.get(position).getContactName());
		}
		holder.description.setText(addresses.get(position).getDescription());
		holder.code.setText(Util.modifyNavCode(addresses.get(position).getCode()));
		holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onIconsClickListener != null) {
					onIconsClickListener.deleteAddress(position);
				}
			}
		});
		Picasso
				.with(this.context)
				.load(addresses.get(position).getAddressPhoto())
				.error(R.drawable.def_image)
				.placeholder(R.drawable.def_image)
				.fit()
				.centerCrop()
				.transform(new RoundedBorderTransform(30, 20))
				.into(holder.addressPhoto);

		holder.addressPhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onIconsClickListener != null) {
					if(addresses.get(position).getAddressPhoto()!= null)
						onIconsClickListener.zoomPhoto(addresses.get(position).getAddressPhoto());
				}
			}
		});


		if (addresses.get(position).isSelected()) {
			holder.bg.setBackgroundColor(context.getResources().getColor(R.color.orange_light));
			holder.code.setTextColor(context.getResources().getColor(R.color.colorWhite));
		} else {
			holder.bg.setBackgroundColor(context.getResources().getColor(R.color.home_background));
			holder.code.setTextColor(context.getResources().getColor(R.color.orange));
		}
		if (addresses.get(position).isVerified()) {
			holder.verified.setVisibility(View.VISIBLE);
		} else {
			holder.verified.setVisibility(View.GONE);
		}
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onIconsClickListener != null) {
					onIconsClickListener.selectDefault(position);
				}
			}
		});
		return view;
	}

	class Holder {
		TextView streetName, description, code, name;
		ImageView deleteBtn, addressPhoto;
		View bg, verified;

	}

	public interface OnIconsClickListener {
		void deleteAddress(int id);

		void selectDefault(int id);

		void zoomPhoto(String uri);
	}

	public void setOnIconsClickLisener(OnIconsClickListener onIconsClickListener) {
		this.onIconsClickListener = onIconsClickListener;
	}
}
