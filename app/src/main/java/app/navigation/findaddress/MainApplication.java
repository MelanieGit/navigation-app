package app.navigation.findaddress;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import app.navigation.findaddress.model.User;
import app.navigation.findaddress.service.Rest;

/**
 * Created by Artyom on 1/12/18.
 */

public class MainApplication extends Application {
	private static MainApplication mainApplication;
	private static User user;

	private Rest rest;

	public MainApplication() {
		mainApplication = this;
	}

	public static  MainApplication getInstance() {
		return mainApplication;
	}

	public static User getUser(){
		return user;
	}
	public static void setUser(User u){
		user = u;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		rest = new Rest(this);
	}
	public Rest getRest() {
		return rest;
	}
}
