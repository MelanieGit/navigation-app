package app.navigation.findaddress.utils.bus;

import app.navigation.findaddress.model.NavCode;

/**
 * Created by Artyom on 1/25/18.
 */

public class OnGetNavCodeEvent {
	private NavCode navCode;

	public OnGetNavCodeEvent(NavCode navCode) {
		this.navCode = navCode;
	}

	public NavCode getNavCode() {
		return navCode;
	}
}
