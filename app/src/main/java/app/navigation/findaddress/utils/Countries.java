package app.navigation.findaddress.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class Countries {

    public static ArrayList getCountries() {
        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);
        for (String country : countries) {
            System.out.println(country);
        }
        return countries;
    }
}