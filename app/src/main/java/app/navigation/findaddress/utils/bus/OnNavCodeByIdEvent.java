package app.navigation.findaddress.utils.bus;

import app.navigation.findaddress.model.NavCode;

/**
 * Created by Artyom on 1/29/18.
 */

public class OnNavCodeByIdEvent {
	NavCode navCode;

	public OnNavCodeByIdEvent(NavCode navCode) {
		this.navCode = navCode;
	}

	public NavCode getNavCode() {
		return navCode;
	}
}
