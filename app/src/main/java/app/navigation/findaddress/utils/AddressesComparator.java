package app.navigation.findaddress.utils;


import android.os.Build;

import java.util.Comparator;

import app.navigation.findaddress.model.NavCode;

/**
 * Created by Artyom on 2/15/19.
 */

public class AddressesComparator implements Comparator<NavCode> {
	@Override
	public int compare(NavCode o1, NavCode o2) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			return Long.compare(o2.getId(),o1.getId());
		}
		return 0;
	}
}
