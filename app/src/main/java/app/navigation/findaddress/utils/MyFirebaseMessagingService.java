package app.navigation.findaddress.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import app.navigation.findaddress.R;


/**
 * Created by melaniah on 2/23/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().get("title").equals("Nikute has started")) {
            Intent startIntent = new Intent(getApplicationContext(), ForegroundService.class);
            startIntent.putExtra("message", remoteMessage);
            startService(startIntent);
        } else {
            NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new android.support.v4.app.NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.icon)
                    .setContentTitle(remoteMessage.getData().get("title"))
                    .setContentText(remoteMessage.getData().get("body"))
                    .setTicker("Nikute event")
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .build();
            nm.notify(0, notification);
        }
    }
}
