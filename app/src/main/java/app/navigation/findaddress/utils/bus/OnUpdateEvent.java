package app.navigation.findaddress.utils.bus;

import app.navigation.findaddress.model.User;

/**
 * Created by Artyom on 1/26/18.
 */

public class OnUpdateEvent {
	private User user;

	public OnUpdateEvent(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}
