package app.navigation.findaddress.utils.bus;

import app.navigation.findaddress.model.ErrorMessage;

/**
 * Created by melaniah on 1/16/2018.
 */

public class ErrorEvent {
    private ErrorMessage errorMessage;

    public ErrorEvent(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}
