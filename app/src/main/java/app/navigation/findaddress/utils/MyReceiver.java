package app.navigation.findaddress.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.ui.ArriveActivity;
import app.navigation.findaddress.ui.HomeActivity;
import app.navigation.findaddress.utils.bus.EventBus;
import app.navigation.findaddress.utils.bus.OnGeoFenceEvent;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        String transition = mapTransition(event.getGeofenceTransition());
        if (transition != null && transition.equals("ENTER")) {
            EventBus.getDefault().post(new OnGeoFenceEvent());
            Intent i = new Intent(context, ArriveActivity.class);
            //  i.setClassName("app.navigation.findaddress", "ui.ArriveActivity");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(i);
        }
    }

    private String mapTransition(int event) {
        switch (event) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return "ENTER";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "EXIT";
            default:
                return "UNKNOWN";
        }
    }
}