package app.navigation.findaddress.utils.bus;

import app.navigation.findaddress.model.User;

public class OnLoginEvent {

	private User user;

	public OnLoginEvent(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}
