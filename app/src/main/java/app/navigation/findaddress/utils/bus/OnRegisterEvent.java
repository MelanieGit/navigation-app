package app.navigation.findaddress.utils.bus;

import app.navigation.findaddress.model.User;

/**
 * Created by Artyom on 1/15/18.
 */
public class OnRegisterEvent {

    private User user;

    public OnRegisterEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}
