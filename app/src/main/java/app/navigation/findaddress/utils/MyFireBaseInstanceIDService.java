package app.navigation.findaddress.utils;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import app.navigation.findaddress.service.Rest;
import app.navigation.findaddress.utils.preference.Preference;

/**
 * Created by melaniah on 2/23/2018.
 */

public class MyFireBaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Preference.writePreference(getApplicationContext(), Constants.TOKEN, refreshedToken);
    }
}
