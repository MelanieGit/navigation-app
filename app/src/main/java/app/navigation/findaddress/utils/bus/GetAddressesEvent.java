package app.navigation.findaddress.utils.bus;

import java.util.ArrayList;

import app.navigation.findaddress.model.NavCode;

/**
 * Created by Artyom on 2/1/18.
 */

public class GetAddressesEvent {
	private ArrayList<NavCode> addresses;

	public GetAddressesEvent(ArrayList<NavCode> addresses) {
		this.addresses = addresses;
	}

	public ArrayList<NavCode> getAddresses() {
		return addresses;
	}
}
