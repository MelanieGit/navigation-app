package app.navigation.findaddress.utils;

/**
 * Created by Artyom on 1/12/18.
 */

public class Constants {
	public static final int ERROR_CODE = 400;
	public static final int SUCCESS_CODE = 200;
	public static final String PASSWORD = "password";
	public static final String EMAIL = "email";
	public static final String FIRST_NAME = "firstName";
	public static final String SUR_NAME = "surName";
	public static final String GENDER = "gender";
	public static final String COUNTRY = "country";
	public static final String MOBILE = "mobile";
	public static final String BUSINESS_NAME = "businessName";
	public static final String COMPANY = "company";
	public static final String NO_INTERNET_CONNECTION = "No internet Connection";
	public static final String USER = "user";
	public static final String LAT = "lat";
	public static final String LNG = "lng";
	public static final String STREET_NAME = "streetName";
	public static final String AREA_NAME = "areaName";
	public static final String CITY = "city";
	public static final String OWNER_ID = "ownerId";
	public static final String DESCRIPTION = "description";
	public static final String LOCATION = "location";
	public static final String ID = "code";
	public static final String NIKUTE = "nikute";
	public static final String ADDRESS_LIST = "addressList";
	public static final String CODE = "code";
	public static final int REQUEST_CODE = 2;
	public static final String CONTACT_NAME = "contactName";
	public static final String CONTACT_NUMBER = "contactNumber";
	public static final String NAV_CODE_ID = "navCodeId";
	public static final String TOKEN = "notificationToken";
	public static final String REQUESTER_ID = "requesterId";
	public static final String USER_ID = "id";
	public static final String STATUS = "status";
	public static final String ENTER_DATE = "enterDate";
	public static final String NAVCODE_ID = "navCodeId";
	public static final String ADDRESS_PHOTO = "addressPhoto";
}
