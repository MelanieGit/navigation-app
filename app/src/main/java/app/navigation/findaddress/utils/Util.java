package app.navigation.findaddress.utils;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Artyom on 1/19/18.
 */

public class Util {
    public static void hideSoftKeyboard(Activity activity) {
        try {
            if (activity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String modifyNavCode(String navCode) {
        String digits = navCode.substring(navCode.length() - 6, navCode.length());
        String digitsWithSpaces = " ";
        for (int i = 0; i < digits.length(); i++) {
            if (i == 3) {
                digits = digits.substring(0, i) + " " + digits.substring(i, digits.length());
            }
        }
        return navCode.substring(0, navCode.length() - 6) + " " + digits;
    }

    public static String removeSpaces(String code) {
        for (int i = 0; i < code.length(); i++) {
            if (code.charAt(i) == ' ') {
                code = code.substring(0, i) + code.substring(i + 1, code.length());
                i--;
            }
        }
        return code;
    }
    public static boolean appInstalledOrNot(String uri, Activity activity) {
        PackageManager pm = activity.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    public static String getVersionCode(Activity activity){
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
           return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

}
