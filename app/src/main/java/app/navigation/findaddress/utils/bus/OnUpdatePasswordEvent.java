package app.navigation.findaddress.utils.bus;

import app.navigation.findaddress.model.User;

/**
 * Created by Artyom on 4/27/18.
 */

public class OnUpdatePasswordEvent {
	User user;
	public OnUpdatePasswordEvent(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}
