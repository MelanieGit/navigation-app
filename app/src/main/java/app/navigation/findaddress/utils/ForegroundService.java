package app.navigation.findaddress.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

import app.navigation.findaddress.R;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import pl.charmas.android.reactivelocation2.ReactiveLocationProviderConfiguration;

/**
 * Created by Artyom on 11/4/18.
 */

public class ForegroundService extends Service {
    private LatLng latLng;
    private ReactiveLocationProvider reactiveLocationProvider;
    private ReactiveLocationProvider locationProvider;
    private Observable<Location> lastKnownLocationObservable;
    private Observable<Location> locationUpdatesObservable;
    private Observable<String> addressObservable;


    private GPSTracker gpsTracker;
    private PendingIntent mGeofencePendingIntent;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
try {
    RemoteMessage remoteMessage = intent.getParcelableExtra("message");

    NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
    Notification notification = new android.support.v4.app.NotificationCompat.Builder(getApplicationContext())
            .setSmallIcon(R.drawable.icon)
            .setContentTitle(remoteMessage.getData().get("title"))
            .setContentText(remoteMessage.getData().get("body"))
            .setTicker("Nikute event")
            .setDefaults(Notification.DEFAULT_SOUND)
            .build();
    nm.notify(0, notification);
    startForeground(0,
            notification);

    reactiveLocationProvider = new ReactiveLocationProvider(this);

    locationProvider = new ReactiveLocationProvider(getApplicationContext(), ReactiveLocationProviderConfiguration
            .builder()
            .setRetryOnConnectionSuspended(true)
            .build()
    );
    lastKnownLocationObservable = locationProvider
            .getLastKnownLocation()
            .observeOn(AndroidSchedulers.mainThread());

    final LocationRequest locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setNumUpdates(5)
            .setInterval(100);

    locationUpdatesObservable = locationProvider
            .checkLocationSettings(
                    new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest)
                            .setAlwaysShow(true)
                            .build()
            )
            .doOnNext(new Consumer<LocationSettingsResult>() {
                @Override
                public void accept(LocationSettingsResult locationSettingsResult) {
                    Status status = locationSettingsResult.getStatus();
                    if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        System.out.println("");
                    }
                }
            })
            .flatMap(new Function<LocationSettingsResult, Observable<Location>>() {
                @Override
                public Observable<Location> apply(LocationSettingsResult locationSettingsResult) {
                    return locationProvider.getUpdatedLocation(locationRequest);
                }
            })
            .observeOn(AndroidSchedulers.mainThread());

    addressObservable = locationProvider.getUpdatedLocation(locationRequest)
            .flatMap(new Function<Location, Observable<List<Address>>>() {
                @Override
                public Observable<List<Address>> apply(Location location) {
                    return locationProvider.getReverseGeocodeObservable(location.getLatitude(), location.getLongitude(), 1);
                }
            })
            .map(new Function<List<Address>, Address>() {
                @Override
                public Address apply(List<Address> addresses) {
                    return addresses != null && !addresses.isEmpty() ? addresses.get(0) : null;
                }
            })
            .map(new AddressToStringFunc())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());

    addGeofence(remoteMessage);
}catch (Exception e){

}
        return super.onStartCommand(intent, flags, startId);
    }

    private void addGeofence(RemoteMessage remoteMessage) {
        final GeofencingRequest geofencingRequest = createGeofencingRequest(remoteMessage);
        if (geofencingRequest == null) return;

        final PendingIntent pendingIntent = getGeofencePendingIntent(remoteMessage);

        reactiveLocationProvider
                .removeGeofences(pendingIntent)
                .flatMap(new Function<Status, Observable<Status>>() {
                    @Override
                    public Observable<Status> apply(Status status) throws Exception {
                        return reactiveLocationProvider.addGeofences(pendingIntent, geofencingRequest);
                    }

                })
                .subscribe(new Consumer<Status>() {
                    @Override
                    public void accept(Status addGeofenceResult) {
                        System.out.println();
                        gpsTracker = new GPSTracker(getApplicationContext());
                        if (gpsTracker.getIsGPSTrackingEnabled()) {
                            gpsTracker.updateGPSCoordinates();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        System.out.println();
                    }
                });
    }

    private GeofencingRequest createGeofencingRequest(RemoteMessage remoteMessage) {
        try {
            double latitude = Double.valueOf(remoteMessage.getData().get("lat"));
            double longitude = Double.valueOf(remoteMessage.getData().get("lng"));


            float radius = 62;
            Geofence geofence = new Geofence.Builder()
                    .setRequestId("GEOFENCE")
                    .setCircularRegion(latitude, longitude, radius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build();
            return new GeofencingRequest.Builder().addGeofence(geofence).build();
        } catch (NumberFormatException ex) {
            System.out.println(ex);
            return null;
        }
    }

    private PendingIntent getGeofencePendingIntent(RemoteMessage remoteMessage) {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        intent.putExtra("navcode_id", remoteMessage.getData().get("navcode_id"));
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }
}
